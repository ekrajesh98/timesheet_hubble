from entry import *
from entry.models import *

@app.route('/homepage')
def home_page():
    return render_template('home_page.html')

@app.route('/register',methods=['POST'])
def register(): 
    if request.method=='POST':
        first_name=request.form['first_name']
        last_name=request.form['last_name']
        user_email=request.form['email']
        password=request.form['password']
        confirm_password=request.form['confirm_password']
        email_list=[]
        email_list1=[]
        email_list=User.query.all()
        for values in email_list:
             email_list1.append(values.email_id)
        if user_email not in email_list1:
            if password==confirm_password:
                user_details=User(first_name=first_name,last_name=last_name,email_id=user_email,password=password)
                db.session.add(user_details)
                db.session.commit()
                return render_template('show_status.html',status='Registration successful')
            else:
                return render_template('show_status.html',status="Password and confirm password doesn't match")
        elif user_email in email_list:
            return render_template('show_status.html',status='User already registered. Please sign-in')
    return render_template('registration_page.html')

@app.route('/login',methods=['GET','POST'])
def login():
    if request.method=='POST':
        email=request.form['email_id']
        password=request.form['password']
        email_list=[]
        email_list1=[]
        email_list=User.query.all()
        
        for var in email_list:
            if email==var.email_id:
                if password==var.password:
                    if email=='admin@mallow-tech.com':
                        return render_template('admin_page.html')
                    user_id=var.user_id
                    return redirect(url_for('timesheet',id=user_id))
                else:
                    return render_template('show_status.html',status='Please enter correct password')
        return render_template('show_status.html',status="Email id doesn't exist")

    return render_template('login_page.html')

@app.route('/timesheet/<id>',methods=['GET','POST'])
def timesheet(id):
    if request.method=='POST':
        user_id=request.form['user_id']
        timesheet_date=datetime.date(datetime.strptime(request.form['date'],'%Y-%m-%d'))
        description=request.form['description']
        project_id=request.form['project_id']
        module_id=request.form['module_id']
        task_id=request.form['task_id']
        worked_hours=request.form['worked_hours']
        time_of_entry=datetime.now()

        entry_data=Timesheet_entry(timesheet_date=timesheet_date,description=description,worked_hours=worked_hours,time_of_entry=time_of_entry,user_id=user_id,module_id=module_id,task_id=task_id,project_id=project_id)
        db.session.add(entry_data)
        db.session.commit()
        return redirect(url_for('timesheet',id=user_id))


    user_query=User.query.filter_by(user_id=id).one()
    first_name=user_query.first_name
    last_name=user_query.last_name

    project_data=Project.query.all()
    module_data=Module.query.filter_by(user_id=id).all()
    task_data=Task.query.filter_by(user_id=id).all()

    worked_hours_dict={0.25:'0:15',
                   0.50:'0:30',
                   0.75:'0:45',
                   1.00:'1:00',
                   1.25:'1:15',
                   1.50:'1:30',
                   1.75:'1:45',
                   2.00:'2:00',
                   2.25:'2:15',
                   2.50:'2:30',
                   2.75:'2:45',
                   3.00:'3:00',
                   }

    # timesheet_data=db.session.query(Timesheet_entry.timesheet_id,Timesheet_entry.user_id,Timesheet_entry.timesheet_date,Timesheet_entry.description,Project.project_name,Module.module_name,Task.task_name,Timesheet_entry.worked_hours)\
    #     .select_from(Timesheet_entry)\
    #         .join(User, User.user_id==Timesheet_entry.user_id).join(Project,Project.project_id==Timesheet_entry.project_id).join(Module,Module.module_id==Timesheet_entry.module_id).join(Task,Task.task_id==Timesheet_entry.task_id).filter(Timesheet_entry.user_id==id).order_by(desc(Timesheet_entry.timesheet_date))

    timesheet_data=Timesheet_entry.query.filter_by(user_id=id).order_by(desc(Timesheet_entry.timesheet_date))

    return render_template('timesheet.html',id=id,f_name=first_name,l_name=last_name,projects=project_data,modules=module_data,tasks=task_data,dt=str(date.today().strftime("%d-%b-%Y")),t_data=timesheet_data,worked_hours_dict=worked_hours_dict)

@app.route('/project',methods=['GET','POST'])
def project():
    if request.method=='POST':
        project_name=request.form['project_name']
        projects=Project.query.all()
        project_available=False
        for var in projects:
            if project_name == var.project_name:
                project_available=True
        if project_available==False:
            project=Project(project_name=project_name)
            db.session.add(project)
            db.session.commit()
            return render_template('show_status.html',status='Project added successfully!!!')
        elif project_available==True:
            return render_template('show_status.html',status='Project already available!!!')
    return render_template("add_project.html")

@app.route('/module/<user_id>',methods=['GET','POST'])
def module(user_id):
    message=None
    if request.method=='POST':
        user_id=request.form['user_id']
        module_name=request.form['module_name']
        modules=Module.query.filter_by(user_id=user_id).all()
        module_available=False
        for var in modules:
            if module_name == var.module_name:
                module_available=True
                flash("Module already available","commit_status_fail")
        if module_available==False:
            module=Module(module_name=module_name,user_id=user_id)
            db.session.add(module)
            db.session.commit()
            flash("Module added successfully","commit_status_success")
        return redirect(url_for('timesheet',id=user_id))
    return render_template("add_module.html",u_id=user_id)

@app.route('/task/<user_id>',methods=['GET','POST'])
def task(user_id):
    if request.method=='POST':
        user_id=request.form['user_id']
        task_name=request.form['task_name']
        tasks=Task.query.filter_by(user_id=user_id).all()
        task_available=False
        for var in tasks:
            if task_name == var.task_name:
                task_available=True
                flash("Task already available","commit_status_fail")
        if task_available==False:
            task=Task(task_name=task_name,user_id=user_id)
            db.session.add(task)
            db.session.commit()
            flash("Task added successfully","commit_status_success")
        return redirect(url_for('timesheet',id=user_id))
    return render_template("add_task.html",u_id=user_id)


@app.route('/edit/<int:timesheet_id>',methods=['GET','POST'])
def edit_entry(timesheet_id):
    timesheet_data=Timesheet_entry.query.filter_by(timesheet_id=timesheet_id).one()
    if request.method=='POST':
        timesheet_data.description=request.form['description']
        timesheet_data.project_id=request.form['project_id']
        timesheet_data.module_id=request.form['module_id']
        timesheet_data.task_id=request.form['task_id']
        timesheet_data.worked_hours=request.form['worked_hours']
        timesheet_data.time_of_entry=datetime.now()
        try:
            db.session.commit()
            flash("The changes have been successfully saved","commit_status_success")
            return redirect(url_for('timesheet',id=timesheet_data.user_id))
        except:
            flash("There is a problem in saving the changes","commit_status_fail")
            return redirect(url_for('timesheet',id=timesheet_data.user_id))
    project_data=Project.query.all()
    module_data=Module.query.filter(Module.user_id==timesheet_data.user_id).all()
    task_data=Task.query.filter(Task.user_id==timesheet_data.user_id).all()
    worked_hours_dict={0.25:'0:15',
                   0.50:'0:30',
                   0.75:'0:45',
                   1.00:'1:00',
                   1.25:'1:15',
                   1.50:'1:30',
                   1.75:'1:45',
                   2.00:'2:00',
                   2.25:'2:15',
                   2.50:'2:30',
                   2.75:'2:45',
                   3.00:'3:00',
                   }
    return render_template('edit_entry.html',data=timesheet_data,projects=project_data,modules=module_data,tasks=task_data,worked_hours_dict=worked_hours_dict) 



@app.route('/delete/<int:timesheet_id>',methods=['GET','POST'])
def delete_entry(timesheet_id):
    timesheet_data=Timesheet_entry.query.filter_by(timesheet_id=timesheet_id).one()
    try:
        db.session.delete(timesheet_data)
        db.session.commit()
    except:
        flash("There is a problem in saving the changes","commit_status")
    return redirect(url_for('timesheet',id=timesheet_data.user_id))
