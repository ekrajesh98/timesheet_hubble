from flask import Flask,render_template,request,url_for,redirect,jsonify,flash
from flask_sqlalchemy import SQLAlchemy
from datetime import date,datetime
from sqlalchemy import desc


app=Flask(__name__,instance_relative_config=True)

app.secret_key = "abc"

app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///timesheet.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db=SQLAlchemy(app)
from entry import models
from entry import views