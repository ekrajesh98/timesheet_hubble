from entry import db

class User(db.Model):
    user_id=db.Column(db.Integer,primary_key=True,nullable=False)
    first_name=db.Column(db.String(30),nullable=False)
    last_name=db.Column(db.String(30),nullable=False)
    email_id=db.Column(db.String(100),nullable=False,unique=True)
    password=db.Column(db.String(100),nullable=False,unique=True)

    entries=db.relationship('Timesheet_entry', backref='user')
    modules=db.relationship('Module',backref='user')
    tasks=db.relationship('Task',backref='user')

class Project(db.Model):
    project_id=db.Column(db.Integer,primary_key=True,nullable=False)
    project_name=db.Column(db.String(100),nullable=False,unique=True)

    entries=db.relationship('Timesheet_entry', backref='project')

class Module(db.Model):
    module_id=db.Column(db.Integer,primary_key=True,nullable=False)
    module_name=db.Column(db.String(100),nullable=False)

    user_id=db.Column(db.Integer,db.ForeignKey('user.user_id'),nullable=False)
    entries=db.relationship('Timesheet_entry', backref='module')

class Task(db.Model):
    task_id=db.Column(db.Integer,primary_key=True,nullable=False)
    task_name=db.Column(db.String(100),nullable=False)

    user_id=db.Column(db.Integer,db.ForeignKey('user.user_id'),nullable=False)
    entries=db.relationship('Timesheet_entry', backref='task')

class Timesheet_entry(db.Model):
    timesheet_id=db.Column(db.Integer,primary_key=True,nullable=False)
    timesheet_date=db.Column(db.Date,nullable=False)
    description=db.Column(db.String(500),nullable=False)
    worked_hours=db.Column(db.Float,nullable=False)
    time_of_entry=db.Column(db.DateTime, nullable=False)

    user_id=db.Column(db.Integer,db.ForeignKey('user.user_id'),nullable=False)
    module_id=db.Column(db.Integer,db.ForeignKey('module.module_id'),nullable=False)
    task_id=db.Column(db.Integer,db.ForeignKey('task.task_id'),nullable=False)
    project_id=db.Column(db.Integer,db.ForeignKey('project.project_id'),nullable=False)